package org.bentosoft.page;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.bentosoft.application.WicketApplication;

/**
 * Classe de Configuração da Home Page.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.org)
 *
 */
public class HomePage extends WebPage
{

    private static final long serialVersionUID = 4677723192874817547L;

    private static final Logger LOGGER = Logger.getLogger(HomePage.class.getName());

    private static final String ID_FORM = "form";

    private static final String ID_BUTTON = "button";

    private static final String ID_SERIAL_PORT_TEXTFIELD = "serialPort";

    protected TextField<String> serialPort;

    public HomePage()
    {
        super();
    }

    @Override
    protected void onInitialize()
    {
        super.onInitialize();
        setOutputMarkupId(true);
        addOrReplace(new Form<Void>(ID_FORM)
        {

            private static final long serialVersionUID = -9211175535649277977L;

            @Override
            protected void onInitialize()
            {
                super.onInitialize();
                addOrReplace(serialPort = new TextField<String>(ID_SERIAL_PORT_TEXTFIELD, Model.of(((WicketApplication) WicketApplication.get()).getSerialPortName())));
                addOrReplace(new AjaxButton(ID_BUTTON, this)
                {

                    private static final long serialVersionUID = -2132838802704678574L;

                    @Override
                    protected void onInitialize()
                    {
                        super.onInitialize();
                        setDefaultFormProcessing(true);
                    }

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form)
                    {
                        try
                        {
                            ((WicketApplication) WicketApplication.get()).setSerialPortName(serialPort.getModelObject());
                            setDefaultModel(Model.of(((WicketApplication) WicketApplication.get()).getSerialPortName()));
                        }
                        catch (Exception e)
                        {
                            LOGGER.log(Level.SEVERE, e.getMessage());
                        }
                        target.add(HomePage.this);
                    }
                });
            }
        });
    }
}
