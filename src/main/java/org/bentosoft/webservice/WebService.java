package org.bentosoft.webservice;

import java.io.IOException;
import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.bentosoft.application.WicketApplication;

/**
 * Classe de configuração do Web Service.
 * 
 * Frameworks: Jersey
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
@Path("/execute")
public class WebService implements Serializable
{

    private static final long serialVersionUID = -125090956529413490L;

    private static final int STATUS_FAIL = 201;

    private static final int STATUS_SUCCESS = 200;

    private static final String MSG_SUCCESS = "<success>Command Executed Successfully.</success>";

    private static final String ERROR_INI = "<error>";

    private static final String ERROR_END = "</error>";

    private static final String ASCII = "ASCII";

    private static final char NEXT_LINE = '\n';

    @GET
    @Path("/{param}")
    public Response execute(@PathParam("param") String command)
    {
        try
        {
            WicketApplication.getConnection().getOut().write(new StringBuilder().append(command).append(NEXT_LINE).toString().getBytes(ASCII));
            return Response.status(STATUS_SUCCESS).entity(MSG_SUCCESS).build();
        }
        catch (IOException e)
        {
            return Response.status(STATUS_FAIL).entity(new StringBuilder().append(ERROR_INI).append(e.getMessage()).append(ERROR_END).toString()).build();
        }
    }

}
