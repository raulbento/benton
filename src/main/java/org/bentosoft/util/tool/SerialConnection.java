package org.bentosoft.util.tool;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

/**
 * Classe de conexão com a porta serial.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
public class SerialConnection implements Serializable
{

    private static final long serialVersionUID = 5506817702010067524L;

    private static final Logger LOGGER = Logger.getLogger(SerialConnection.class.getName());

    private InputStream in;

    private OutputStream out;

    private SerialPort serialPort;

    public SerialConnection()
    {
        super();
    }

    public void connect(String portName) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if (portIdentifier.isCurrentlyOwned())
        {
            LOGGER.log(Level.SEVERE, "Port is currently in use.");
        }
        else
        {
            CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);
            if (commPort instanceof SerialPort)
            {
                serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                in = serialPort.getInputStream();
                out = serialPort.getOutputStream();
                new Thread(new SerialWriter(out)).start();
                serialPort.addEventListener(new SerialReader(in));
                serialPort.notifyOnDataAvailable(true);
            }
            else
            {
                LOGGER.log(Level.SEVERE, "Only serial ports are handled by this example.");
            }
        }
    }

    public InputStream getIn()
    {
        return in;
    }

    public void setIn(InputStream in)
    {
        this.in = in;
    }

    public OutputStream getOut()
    {
        return out;
    }

    public void setOut(OutputStream out)
    {
        this.out = out;
    }

    public static class SerialReader implements SerialPortEventListener
    {

        private static final char NEXT_LINE = '\n';

        private InputStream in;

        private byte[] buffer = new byte[1024];

        public SerialReader(InputStream in)
        {
            this.in = in;
        }

        @Override
        public void serialEvent(SerialPortEvent event)
        {
            switch (event.getEventType())
            {
                case SerialPortEvent.DATA_AVAILABLE:
                    int data;

                    try
                    {
                        int len = 0;
                        while ((data = in.read()) > -1)
                        {
                            if (data == NEXT_LINE)
                            {
                                break;
                            }
                            buffer[len++] = (byte) data;
                        }
                        String comando = new String(buffer, 0, len);
                        JerseyClient.sendAndonCommand(comando);
                        LOGGER.log(Level.INFO, comando);
                    }
                    catch (IOException e)
                    {
                        LOGGER.log(Level.SEVERE, e.getMessage());
                        System.exit(-1);
                    }
                    break;

                default:
                    break;
            }

        }

    }

    public static class SerialWriter implements Runnable
    {

        OutputStream out;

        public SerialWriter(OutputStream out)
        {
            this.out = out;
        }

        @Override
        public void run()
        {
            try
            {
                int c = 0;
                while ((c = System.in.read()) > -1)
                {
                    this.out.write(c);
                }
            }
            catch (IOException e)
            {
                LOGGER.log(Level.SEVERE, e.getMessage());
                System.exit(-1);
            }
        }
    }

    public static void main(String[] args)
    {
        try
        {
            SerialConnection connection = new SerialConnection();
            connection.connect("COM5");
            connection.out.write("RST,029,X\n".toString().getBytes("ASCII"));

        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

}
