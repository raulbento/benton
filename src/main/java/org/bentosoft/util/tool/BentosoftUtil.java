package org.bentosoft.util.tool;

import java.io.Serializable;

import org.bentosoft.util.enums.SistemaOperacional;

/**
 * Utilitário padrão Bentosoft.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
public class BentosoftUtil implements Serializable
{

    private static final long serialVersionUID = 4964340928682529329L;

    private static final String OS_PROPERTY = "os.name";

    private static final String SUNOS = "sunos";

    private static final String MAC = "mac";

    private static final String AIX = "aix";

    private static final String NUX = "nux";

    private static final String NIX = "nix";

    private static final String WIN = "win";

    private static SistemaOperacional sistemaOperacional;

    public static SistemaOperacional getSistemaOperacional()
    {
        return getSistemaOperacional(System.getProperty(OS_PROPERTY));
    }

    public static SistemaOperacional getSistemaOperacional(String os)
    {
        if (sistemaOperacional == null)
        {
            if (os.toLowerCase().indexOf(WIN) >= 0)
            {
                sistemaOperacional = SistemaOperacional.WINDOWS;
            }
            else if (os.toLowerCase().indexOf(NIX) >= 0 || os.toLowerCase().indexOf(NUX) >= 0 || os.toLowerCase().indexOf(AIX) > 0)
            {
                sistemaOperacional = SistemaOperacional.LINUX;
            }
            else if (os.toLowerCase().indexOf(MAC) >= 0)
            {
                sistemaOperacional = SistemaOperacional.OSX;
            }
            else if (os.toLowerCase().indexOf(SUNOS) >= 0)
            {
                sistemaOperacional = SistemaOperacional.SOLARIS;
            }
            else
            {
                sistemaOperacional = SistemaOperacional.DESCONHECIDO;
            }
        }
        return sistemaOperacional;
    }

}
