package org.bentosoft.util.tool;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Classe de conexão com a porta serial.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
public class JerseyClient implements Serializable
{

    private static final long serialVersionUID = 2887651066310729974L;

    private static final Logger LOGGER = Logger.getLogger(JerseyClient.class.getName());

    private static final String HTTP = "http://";

    private static final String POST_ADD = "/avalcontrole/service";

    public static void sendAndonCommand(String command)
    {
        try
        {

            WebResource webResource = Client.create().resource(new StringBuilder().append(HTTP).append(getAddress(command.substring(3, command.lastIndexOf(",")))).append(POST_ADD).toString()).path("andon").path("novoalerta").path(command);

            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

            if (response.getStatus() != 200)
            {
                LOGGER.log(Level.SEVERE, "Only serial ports are handled by this example.");
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
            String output = response.getEntity(String.class);
            LOGGER.log(Level.INFO, output);
        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    private static String getAddress(String string)
    {
        switch (BentosoftUtil.getSistemaOperacional())
        {
            case WINDOWS:
                return "localhost:8080";
            case LINUX:

                return "";
            default:
                return null;
        }
    }

}
