package org.bentosoft.util.enums;

/**
 * Tipos de Sistemas Operacionais.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
public enum SistemaOperacional
{
    WINDOWS("Windows", 0), LINUX("Linux", 1), OSX("OSX", 2), SOLARIS("Solaris", 3), DESCONHECIDO("Desconhecido", 4);

    private String descricao;

    private Integer valor;

    private SistemaOperacional(String descricao, Integer valor)
    {
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public Integer getValor()
    {
        return valor;
    }

    public static SistemaOperacional getSistemaOperacional(int valor)
    {
        switch (valor)
        {
            case 0:
                return WINDOWS;
            case 1:
                return LINUX;
            case 2:
                return OSX;
            case 3:
                return SOLARIS;
            default:
                return DESCONHECIDO;
        }
    }

}
