package org.bentosoft.util.enums;

/**
 * Tipos de Botões do Andon.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.net)
 *
 */
public enum BotaoAndon
{
    A("A"), B("B"), C("C"), X("Todos");

    private String descricao;

    private BotaoAndon(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
