package org.bentosoft.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.bentosoft.page.HomePage;
import org.bentosoft.util.tool.SerialConnection;

/**
 * Application Class.
 * 
 * @author Raul Silveira Bento (raul@bentosoft.org)
 *
 */
public class WicketApplication extends WebApplication implements Serializable
{

    private static final long serialVersionUID = 7367050939130937917L;

    private static final Logger LOGGER = Logger.getLogger(WicketApplication.class.getName());

    private static final String INVALID = "Invalid";

    private static final String CONNECTION_COMMENT = "New Serial Port Set";

    private static final String DEFAULT_PORT = "COM3";

    private static final String ST_SERIAL_PORT = "SerialPort";

    private static final String PROPERTIES_FILE_NAME = "benton.properties";

    private static File file = new File(PROPERTIES_FILE_NAME);

    private static SerialConnection connection = new SerialConnection();

    public static SerialConnection getConnection()
    {
        return connection;
    }

    private String serialPortName;

    private void connectToSerialPort()
    {
        try
        {
            connection.connect(getSerialPortName());
        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    @Override
    public Class<? extends WebPage> getHomePage()
    {
        return HomePage.class;
    }

    public String getSerialPortName()
    {
        return serialPortName;
    }

    @Override
    public void init()
    {
        super.init();
        try
        {
            loadPorperties();
        }
        catch (IOException e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        connectToSerialPort();
    }

    private void loadPorperties() throws IOException
    {
        InputStream inputStream = null;
        try
        {
            if (!file.exists())
            {
                file.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(file);
                Properties properties = new Properties();
                properties.setProperty(ST_SERIAL_PORT, DEFAULT_PORT);
                properties.store(outputStream, CONNECTION_COMMENT);
                outputStream.flush();
                outputStream.close();
            }
            inputStream = new FileInputStream(file);
        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
            inputStream = null;
        }
        try
        {
            if (inputStream == null)
            {
                inputStream = getClass().getResourceAsStream(PROPERTIES_FILE_NAME);
            }
            Properties props = new Properties();
            props.load(inputStream);
            serialPortName = props.getProperty(ST_SERIAL_PORT);
        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, e.getMessage());
            serialPortName = INVALID;
        }
        finally
        {
            if (inputStream != null)
            {
                inputStream.close();
            }
        }
    }

    public void setSerialPortName(String serialPortName) throws Exception
    {
        try
        {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            Properties properties = new Properties();
            properties.setProperty(ST_SERIAL_PORT, serialPortName);
            properties.store(fileOutputStream, CONNECTION_COMMENT);
            fileOutputStream.flush();
            fileOutputStream.close();
            this.serialPortName = serialPortName;
        }
        finally
        {
            getConnection().connect(this.serialPortName);
        }
    }

}
